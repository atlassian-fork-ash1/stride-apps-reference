const express = require('express');
const router = express.Router();
const logger = require('../middleware/logger').logger;
const helpers = require('../helpers');
const stride = require('../client');
let https = require('https');

//simple utility function to download an image from a website
async function downloadImage(imgUrl) {
  return new Promise(resolve => {
    https.get(imgUrl, function(stream) {
      resolve(stream);
    });
  });
}

/**
 * @name Media: Send a message with a file or image
 * @description
 * To send a message with a file, first upload the file or image to the conversation, using the send a file endpoint.
 * Regardless of media type, you must set the content type of the file to application/octet-stream in the Content-Type HTTP header.
 * Media file must be a base64 data uri string. Headers must set content-type of application/octet-stream.
 *
 * You can then send a message including a media node referencing the uploaded file
 *
 * @see {@link https://developer.atlassian.com/cloud/stride/rest/#rest-site-cloudId-conversation-conversationId-media-post |API Reference: Media Upload }
 * @see {@link https://developer.atlassian.com/cloud/stride/learning/sending-media/ | How-to Guide }
 * @see {@link https://developer.atlassian.com/cloud/stride/apis/document/nodes/mediaGroup/ | MediaGroup Message Node }
 *
 */

router.get('/mediaMessage', async (req, res, next) => {
  const loggerInfoName = 'media_file_upload';

  try {
    const { cloudId, conversationId } = res.locals.context;

    // first, download an image from Giphy
    var stream = await downloadImage('https://media.giphy.com/media/L12g7V0J62bf2/giphy.gif');
    const opts = {
      headers: { 'Content-Type': 'application/octet-stream' },
      body: stream,
    };

    // Then, upload it to Stride
    var uploadString = await stride.api.media.upload(cloudId, conversationId, 'an_image.gif', opts);
    var upload = JSON.parse(uploadString);

    logger.info(`${loggerInfoName} media upload successful ${upload.data.id}`);

    // Finally, send a message including a reference to the uploaded image
    let messageWithImage = helpers.format.mediaFormat(conversationId, upload.data.id);
    const msgOpts = {
      body: messageWithImage,
    };
    await stride.api.messages.sendMessage(cloudId, conversationId, msgOpts);

    res.sendStatus(204);
  } catch (err) {
    logger.error(`${loggerInfoName} found error: ${err}`);
    res.sendStatus(500);
    next(err);
  }
});

router.get('/mediaDelete', async (req, res, next) => {
  const loggerInfoName = 'media_delete';

  try {
    const { cloudId, conversationId } = res.locals.context;

    let stream = await downloadImage('https://media.giphy.com/media/L12g7V0J62bf2/giphy.gif');

    // First, upload the file
    const opts = {
      headers: { 'Content-Type': 'application/octet-stream' },
      body: stream,
    };
    let upload = await stride.api.media.upload(cloudId, conversationId, 'an_image.jpg', opts);
    logger.info(`${loggerInfoName} media upload successful ${upload}`);

    // Then delete it
    await stride.api.media.delete(cloudId, conversationId, upload.data.id, {});
    logger.info(`${loggerInfoName} media delete successful`);

    res.sendStatus(204);
  } catch (err) {
    logger.error(`${loggerInfoName} found error: ${err}`);
    res.sendStatus(500);
    next(err);
  }
});

module.exports = router;
