const express = require('express');
const router = express.Router();
const logger = require('../middleware/logger').logger;
const stride = require('../client');

/**
 * @name Conversations: get details
 * @description
 * Get a conversation's details.
 * @see {@link https://developer.atlassian.com/cloud/stride/rest/#api-site-cloudId-conversation-conversationId-get | API Reference: Get Conversation Details}
 * @see {@link https://developer.atlassian.com/cloud/stride/learning/conversations/ | Concept Guide }
 */
router.get('/getConversationDetails', async (req, res, next) => {
  let loggerInfoName = 'conversation_details';

  try {
    const { cloudId, conversationId } = res.locals.context;
    logger.info(`${loggerInfoName} incoming request for ${conversationId}`);

    const conversation = await stride.api.conversations.get(cloudId, conversationId, {});
    const opts = {
      body: `This conversation's ID is: **${conversation.id}** and its name: **${
        conversation.name
      }**`,
      headers: { 'Content-Type': 'text/markdown' },
    };
    await stride.api.messages.sendMessage(cloudId, conversationId, opts);

    res.sendStatus(204);
  } catch (err) {
    logger.error(`${loggerInfoName} error found: ${err}`);
    res.sendStatus(500);
    next(err);
  }
});

/**
 * @name Conversations: archive
 * @description
 * Archive a conversation
 * @see {@link https://developer.atlassian.com/cloud/stride/rest/#api-site-cloudId-conversation-conversationId-archive-put | API Reference: Put Conversation Archive }
 * @see {@link https://developer.atlassian.com/cloud/stride/learning/conversations/ | Concept Guide }
 */
router.post('/archiveConversation', async (req, res, next) => {
  let loggerInfoName = 'conversation_archive';

  try {
    const { cloudId, conversationId } = res.locals.context;

    logger.info(`${loggerInfoName} incoming request for ${conversationId}`);

    await stride.api.conversations.archive(cloudId, conversationId, { body: {} });
    res.sendStatus(200);
  } catch (err) {
    logger.error(`${loggerInfoName} error found: ${err}`);
    res.sendStatus(500);
    next(err);
  }
});

/**
 * @name Conversations: unarchive
 * @description
 * Unarchive a conversation
 * @see {@link https://developer.atlassian.com/cloud/stride/rest/#api-site-cloudId-conversation-conversationId-unarchive-put | API Reference: Put Conversation UnArchive }
 * @see {@link https://developer.atlassian.com/cloud/stride/learning/conversations/ | Concept Guide }
 */
router.post('/unarchiveConversation', async (req, res, next) => {
  let loggerInfoName = 'conversation_unarchive';

  try {
    const { cloudId, conversationId } = res.locals.context;

    logger.info(`${loggerInfoName}: incoming request for ${conversationId}`);

    await stride.api.conversations.unarchive(cloudId, conversationId, {});
    res.sendStatus(200);
  } catch (err) {
    logger.error(`${loggerInfoName} error found: ${err}`);
    next(err);
  }
});

router.post('/addUser', async (req, res) => {
  let loggerInfoName = 'conversation_add_user';
  const { cloudId, conversationId } = res.locals.context;
  const roomData = {
    body: {
      name: `Stride Reference App : ${new Date().getTime()}`,
      privacy: 'public',
      topic: 'This room was created via the API',
    },
  };

  try {
    // Create a new conversation, get back its ID
    const { id } = await stride.api.conversations.create(cloudId, roomData);

    // Get the list of users from the roster of the current conversation
    const { values } = await stride.api.conversations.getRoster(cloudId, conversationId);

    // Add users from the current conversation to the new one
    await stride.api.conversations.addUsers(cloudId, id, { body: { add: values } });
    res.sendStatus(200);
  } catch (e) {
    logger.error(`${loggerInfoName} Error adding users ${e.message}`);
    res.sendStatus(400);
  }
});

module.exports = router;
