const express = require('express');
const router = express.Router();
const stride = require('../client');
const logger = require('../middleware/logger').logger;
const helpers = require('../helpers');

/**
 *  @name  Webhooks
 *  @alias Webhooks: listening to conversation events
 * @see {@link https://developer.atlassian.com/cloud/stride/apis/modules/chat/webhook/ | Webhooks }
 *  @description
 *
 * Update your descriptor to listen to conversations events like join/leave or created/updated.
 *
 * descriptor.json configuration to listen for conversation events
 *```
 * //descriptor.json entry to listen to conversation events
 chat:webhook
 {
   "key": "webhook-conversationEvents",
   "event": "conversation:updates",
   "url": "/webhooks/conversationEvents"
 }

 */
router.post('/conversationEvent', async (req, res, next) => {
  //for webhooks send the response ASAP, otherwise Stride tries to re-deliver the webhook up to 3 times after timeout
  res.sendStatus(204);
  let loggerInfoName = 'webhooks_conversation_update';

  try {
    const { cloudId, conversationId } = res.locals.context;
    logger.info(`${loggerInfoName} incoming for ${conversationId}: ${req.body.type}`);

    //Send Webhook Message
    let message = helpers.format.messageWithConversationEvent(
      'Conversation Event Webhooks',
      `A Conversation Event occurred and I sent this message.`
    );
    stride.api.messages.sendMessage(cloudId, conversationId, { body: message });
  } catch (err) {
    logger.error(`${loggerInfoName} error found: ${err}`);
    next(err);
  }
});

module.exports = router;
